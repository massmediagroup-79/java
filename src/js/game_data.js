class GameData {
  setSession(iterator, arrField) {
    if (typeof (Storage) !== undefined) {
      sessionStorage.iterator = iterator;
      sessionStorage.arrField = JSON.stringify(arrField);
    }
  }

  setIteratorWithSession() {
    return (sessionStorage.iterator && typeof (Storage) !== undefined) ? sessionStorage.iterator : 0;
  }

  setArrFieldWithSession() {
    if (sessionStorage.arrField && typeof (Storage) !== undefined) {
      this.setVisualGameField(JSON.parse(sessionStorage.arrField));
      return JSON.parse(sessionStorage.arrField);
    }
    return [[,,,], [,,,], [,,,]];
  }
  setVisualGameField(arrField) {
    arrField.forEach((item, index) => {
      item.forEach((iterator, cell) => {
        if (item != undefined && iterator != undefined) {
          if (iterator === 'x') {
            $(`.game td:eq(${index * arrField.length + cell})`).addClass('player1');
          } else {
            $(`.game td:eq(${index * arrField.length + cell})`).addClass('player2');
          }
        }
      });
    });
  }
}

export default GameData;
