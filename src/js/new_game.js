class NewGame {
  constructor(buttonId) {
    $(buttonId).click(() => {
      (typeof (Storage) !== undefined) ? sessionStorage.clear() : 0;
      location.reload(true);
    });
  }
}
export default NewGame;
