class CheckElement {
  constructor(tableId, game) {
    $(() => {
      $(`${tableId} td:not(.player1, .player2)`).on('click', function () {
        const x = $(this).parent('tr').index();
        const y = $(this).parent().children().index($(this));
        $(this).addClass(`player${game.makeMove(x, y)}`);
        if(game.getWinner() === 'draw')
        {
          $(`${tableId} td`).off('click');
          $('#inf-player').text(game.getWinner());
          return;
        }
        else if(game.getWinner()){
          $(`${tableId} td`).off('click');
          $('#inf-player').text(`Win: ${game.getWinner()}`);
          return;
        }
        $(this).off('click');
      });
    });
  }
}
export default CheckElement;
