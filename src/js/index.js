import GameData from './game_data';
import CheckElement from './check_element';
import '../scss/costumeStyle.scss';
import Game from './game';
import NewGame from './new_game';

const gameDataSession = new GameData();
const game = new Game(gameDataSession);

new CheckElement('#click-table', game);
new NewGame('#new-game');


