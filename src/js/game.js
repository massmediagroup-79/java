class Game {
  constructor(gameDataSession) {
    // eslint-disable-next-line no-sparse-arrays
    this.gameDataSession = gameDataSession;
    this.iterator = gameDataSession.setIteratorWithSession();
    this.arrGame = gameDataSession.setArrFieldWithSession();
  }

  makeMove(x, y) {
    if (this.iterator % 2 === 0) this.arrGame[x][y] = 'x';
    else this.arrGame[x][y] = '0';
    const numberPlayer = this.iterator % 2 + 1;
    this.iterator++;
    // eslint-disable-next-line no-undef
    this.gameDataSession.setSession(this.iterator, this.arrGame);
    return numberPlayer;
  }

  checkWinnerByMark(symb) {
    let cols;
    let rows;
    for (let row = 0; row < 3; row++) {
      cols = true;
      rows = true;
      for (let col = 0; col < 3; col++) {
        rows &= (this.arrGame[row][col] === symb);
        cols &= (this.arrGame[col][row] === symb);
      }
      if (rows || cols) return true;
    }
    return (
      (this.arrGame[0][0] === symb && this.arrGame[1][1] === this.arrGame[0][0] && this.arrGame[2][2] === this.arrGame[0][0])
        || (this.arrGame[2][0] === symb && this.arrGame[1][1] === this.arrGame[2][0] && this.arrGame[0][2] === this.arrGame[2][0])
    );
  }

  getWinner() {
    if (this.checkWinnerByMark('x')) { return 'x'; }
    if (this.checkWinnerByMark('0')) { return '0'; }
    if(this.iterator === 9) { return 'draw'; }
    return false;
  }

  getHits() {
    return this.arrGame;
  }
}
export default Game;
